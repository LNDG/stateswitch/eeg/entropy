% Plot results from MSE analysis

% 180129 | JQK adapted function for STSWD study YA

% Note: For study data, channel positions were already rearranged during
% preprocessing.

%% set up paths

clear all; clc; restoredefaultpath;

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/';
pn.dataInOUT  = [pn.root, 'B_data/C_MSE_Output_v2/'];
addpath([pn.root, 'T_tools/fieldtrip-20170904/']);
addpath([pn.root, 'T_tools/brewermap/']);
addpath([pn.root, 'T_tools/mseb/']);
addpath([pn.root, 'T_tools/']);
ft_defaults

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')

%% load data

BLlims = [3 3.2]; % within-condition mean baseline

load([pn.dataInOUT, 'mseavg_CSD_YA.mat'], 'mseavg');
mseavg.datBL=10*log10(mseavg.dat./repmat(nanmean(mseavg.dat(:,:,:,find(mseavg.time>=BLlims(1) & mseavg.time<BLlims(2)),:),4), 1,1,1,numel(mseavg.time),1));
mseavg.rBL=10*log10(mseavg.r./repmat(nanmean(mseavg.r(:,:,:,find(mseavg.time>=BLlims(1) & mseavg.time<BLlims(2)),:),4), 1,1,1,numel(mseavg.time),1));

%% baseline corrected: fast-scale occipital entropy increases with load

scales = 1;
channels = 55:60;

h = figure;
subplot(2,1,1);
    patches.timeVec = [-1.500 0 1.000 3.000 6.000 8.000 9.500];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [-1 -1 1*[1 1]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    colorVec = brewermap(4, 'Dark2');
    for indCond = 1:4
        tmpData = squeeze(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,indCond),3),2));
        tmpMeanData = squeeze(nanmean(tmpData,1));
        tmpSEMData = std(tmpData,[],1)/sqrt(size(tmpData,1));
        lineProps.col = {colorVec(indCond,:)};
        l{indCond} = mseb(mseavg.time, tmpMeanData, tmpSEMData, lineProps, 1); hold on;
    end
    legend([l{1}.mainLine, l{2}.mainLine, l{3}.mainLine, l{4}.mainLine], {'dim1'; 'dim2'; 'dim3'; 'dim4'}); legend('boxoff');
    xticks(mseavg.time(1:10:end))
    title('MSEn during StateSwitch');
    xlim([-1.25,9.25]); %ylim([-1 1]);
    set(findall(gcf,'-property','FontSize'),'FontSize',16);
    xlabel('Time [s]'); ylabel('MSEn [a.u.]')
subplot(2,1,2);
    patches.timeVec = [-1.500 0 1.000 3.000 6.000 8.000 9.500];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [-.3 -.3 .3*[1 1]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    colorVec = brewermap(4, 'Dark2');
    for indCond = 1:4
        tmpData = squeeze(nanmean(nanmean(mseavg.rBL(:,channels,scales,:,indCond),3),2));
        tmpMeanData = squeeze(nanmean(tmpData,1));
        tmpSEMData = std(tmpData,[],1)/sqrt(size(tmpData,1));
        lineProps.col = {colorVec(indCond,:)};
        l{indCond} = mseb(mseavg.time, tmpMeanData, tmpSEMData, lineProps, 1); hold on;
    end
    legend([l{1}.mainLine, l{2}.mainLine, l{3}.mainLine, l{4}.mainLine], {'dim1'; 'dim2'; 'dim3'; 'dim4'}); legend('boxoff');
    xticks(mseavg.time(1:10:end))
    title('R param during StateSwitch');
    xlim([-1.25,9.25]); %ylim([-.3 .3]);
    set(findall(gcf,'-property','FontSize'),'FontSize',16);
    xlabel('Time [s]'); ylabel('R param [a.u.]')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v2/C_figures/';
figureName = 'D_YA_FastScaleTime';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

% figure; imagesc(squeeze(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,4),3)-...
%     nanmean(mseavg.datBL(:,channels,scales,:,1),3),1)))
% 
% figure; imagesc(squeeze(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,3),3)-...
%     nanmean(mseavg.datBL(:,channels,scales,:,1),3),2)))
% 
% figure; imagesc(squeeze(nanmean(nanmean(mseavg.dat(:,channels,scales,:,3),3)-...
%     nanmean(mseavg.dat(:,channels,scales,:,1),3),2)))
% 
% figure; imagesc(squeeze(nanmedian(nanmedian(mseavg.dat(:,channels,scales,:,1),3),2)))
% figure; imagesc(squeeze(nanmedian(nanmedian(mseavg.datBL(:,channels,scales,:,1),3),2)))

%% median split on change

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/C2_attentionFactor_OA/B_data/A_EEGAttentionFactor_YAOA.mat')

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

idx_AttFactor = find(ismember(EEGAttentionFactor.IDs, IDs));
[sortVal, sortIdx] = sort(EEGAttentionFactor.PCAalphaGamma(idx_AttFactor), 'ascend');

lowChIdx = sortIdx(1:floor(numel(sortIdx)/2));
highChIdx = sortIdx(floor(numel(sortIdx)/2)+1:end);

scales = 1;
scales = 13:16;
channels = 51:60;
channels = 1:21;

    l1 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(lowChIdx,channels,scales,17:25,1),5),4),3),2));
    l2 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(lowChIdx,channels,scales,17:25,2),5),4),3),2));
    l3 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(lowChIdx,channels,scales,17:25,3),5),4),3),2));
    l4 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(lowChIdx,channels,scales,17:25,4),5),4),3),2));
    
    l5 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(highChIdx,channels,scales,17:25,1),5),4),3),2));
    l6 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(highChIdx,channels,scales,17:25,2),5),4),3),2));
    l7 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(highChIdx,channels,scales,17:25,3),5),4),3),2));
    l8 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(highChIdx,channels,scales,17:25,4),5),4),3),2));

    figure; bar([nanmedian([l1,l2,l3,l4],1),nanmedian([l5,l6,l7,l8],1)])
    xlabel('Condition [Low/High modulators]'); ylabel('Entropy Scale 1');
    xticklabels({'Low: L1', 'Low: L2', 'Low: L3', 'Low: L4', 'High: L1', 'High: L2', 'High: L3', 'High: L4'})
    title({'Low AMF subjects also have low entropy at baseline'; 'i.e. they should have high alpha (low desync) at baseline'})
    
    for scales = 1:size(mseavg.dat,3)
        l1 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,51:60,scales,17:25,1),5),4),3),2));
        l2 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,51:60,scales,17:25,2),5),4),3),2));
        l3 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,51:60,scales,17:25,3),5),4),3),2));
        l4 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,51:60,scales,17:25,4),5),4),3),2));
        l5 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,51:60,scales,17:25,1),5),4),3),2));
        l6 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,51:60,scales,17:25,2),5),4),3),2));
        l7 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,51:60,scales,17:25,3),5),4),3),2));
        l8 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,51:60,scales,17:25,4),5),4),3),2));
        EntropyBySub(scales,:) = [nanmedian([l1,l2,l3,l4],1),nanmedian([l5,l6,l7,l8],1)];
    end
    
    figure; imagesc(EntropyBySub)
    
    
    for scales = 1:size(mseavg.dat,3)
        l1 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,13:14,scales,17:25,1),5),4),3),2));
        l2 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,13:14,scales,17:25,2),5),4),3),2));
        l3 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,13:14,scales,17:25,3),5),4),3),2));
        l4 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,13:14,scales,17:25,4),5),4),3),2));
        l5 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,13:14,scales,17:25,1),5),4),3),2));
        l6 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,13:14,scales,17:25,2),5),4),3),2));
        l7 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,13:14,scales,17:25,3),5),4),3),2));
        l8 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,13:14,scales,17:25,4),5),4),3),2));
        EntropyBySub(scales,:) = [nanmedian([l1,l2,l3,l4],1),nanmedian([l5,l6,l7,l8],1)];
    end
    
    figure; imagesc(EntropyBySub)
    
    scales = 1;
    %scales = 1:17;
    channels = 44:60;
    %channels = 1:21;
    
    %scales = 1;
    scales = 8:16;
    channels = 13:14;
    channels = [5,6,12,13,14,15,23];
    %channels = 1:21;
    
    figure;
    subplot(2,1,1); hold on;
        plot(squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,channels,scales,:,1),5),3),2),1)));
        plot(squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,channels,scales,:,2),5),3),2),1)));
        plot(squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,channels,scales,:,3),5),3),2),1)));
        plot(squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,channels,scales,:,4),5),3),2),1)));
    subplot(2,1,2); hold on;
        plot(squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,channels,scales,:,1),5),3),2),1)));
        plot(squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,channels,scales,:,2),5),3),2),1)));
        plot(squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,channels,scales,:,3),5),3),2),1)));
        plot(squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,channels,scales,:,4),5),3),2),1)));

    figure;
    subplot(1,2,1);
        bar([squeeze(nanmedian(nanmean(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,channels,scales,17:25,1),4),5),3),2),1)),...
        squeeze(nanmedian(nanmean(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,channels,scales,17:25,2),4),5),3),2),1)),...
        squeeze(nanmedian(nanmean(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,channels,scales,17:25,3),4),5),3),2),1)),...
        squeeze(nanmedian(nanmean(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,channels,scales,17:25,4),4),5),3),2),1))]); ylim([0.8 1])
    subplot(1,2,2);
        bar([squeeze(nanmedian(nanmean(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,channels,scales,17:25,1),4),5),3),2),1)),...
        squeeze(nanmedian(nanmean(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,channels,scales,17:25,2),4),5),3),2),1)),...
        squeeze(nanmedian(nanmean(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,channels,scales,17:25,3),4),5),3),2),1)),...
        squeeze(nanmedian(nanmean(nanmean(nanmean(nanmean(mseavg.dat(lowChIdx,channels,scales,17:25,4),4),5),3),2),1))]); ylim([0.8 1])

    h = figure('units','normalized','position',[.1 .1 .3 .3]);
    cla;
    l1 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,17:25,1),4),5),3),2));
    l2 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,17:25,2),4),5),3),2));
    l3 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,17:25,3),4),5),3),2));
    l4 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,17:25,4),4),5),3),2));
    tmp1 = EEGAttentionFactor.PCAalphaGamma(idx_AttFactor);
    tmp2 = nanmean([l2,l3,l4],2)-l1;
    tmp2(4) = NaN; tmp1(37) = NaN;
    scatter(tmp1, tmp2, 'filled'); lsline();
    xlabel('Attention factor'); ylabel('Right frontal delta MSE 234-1');
    [r,p] = corrcoef(tmp1, tmp2, 'rows', 'complete')
    legend(['Correlation: ', num2str(round(r(2),2)), ' p = ',num2str(round(p(2),3))])
    
    % do the same for linear effects
    
    b = []; b_r = [];
    for indID = 1:47
        for indChan = 1:60
            for indScale = 1:18
                for indTime = 1:36
                    X = [1,1;1,2;1,3;1,4];
                    y = [squeeze(mseavg.datBL(indID,indChan,indScale,indTime,:))];
                    y2 = [squeeze(mseavg.rBL(indID,indChan,indScale,indTime,:))];
                    b(indID,indChan,indScale,indTime,:) = regress(y,X);
                    b_r(indID,indChan,indScale,indTime,:) = regress(y2,X);
                end
            end
        end
    end

    
    scales = 12:18;
    channels = [4,5,6,12,13,14,15,23];
    %channels = [4,5,6,12,13];
    %channels = [1:21];
    %channels = [5];
    
    h = figure('units','normalized','position',[.1 .1 .4 .3]);
    subplot(1,2,1); cla;
        linearEffect = squeeze(nanmean(nanmean(nanmean(nanmean(b(:,channels,scales,17:25,2),4),5),3),2));
        tmp1 = EEGAttentionFactor.PCAalphaGamma(idx_AttFactor);
        tmp2 = linearEffect;
        tmp2(4) = NaN; tmp1(37) = NaN;
        scatter(tmp1, tmp2, 'filled'); lsline();
        xlabel('Attention factor'); ylabel('Right frontal delta MSE 234-1');
        [r,p] = corrcoef(tmp1, tmp2, 'rows', 'complete')
        legend(['Correlation: ', num2str(round(r(2),2)), ' p = ',num2str(round(p(2),3))])
    subplot(1,2,2); cla;
        linearEffect = squeeze(nanmean(nanmean(nanmean(nanmean(b_r(:,channels,scales,17:25,2),4),5),3),2));
        tmp1 = EEGAttentionFactor.PCAalphaGamma(idx_AttFactor);
        tmp2 = linearEffect;
        tmp2(4) = NaN; tmp1(37) = NaN;
        scatter(tmp1, tmp2, 'filled'); lsline();
        xlabel('Attention factor'); ylabel('Right frontal delta MSE 234-1');
        [r,p] = corrcoef(tmp1, tmp2, 'rows', 'complete')
        legend(['Correlation: ', num2str(round(r(2),2)), ' p = ',num2str(round(p(2),3))])
        
    scales = 14:18;
    for channels = 1:60
        linearEffect = squeeze(nanmean(nanmean(nanmean(nanmean(b(:,channels,scales,17:25,2),4),5),3),2));
        tmp1 = EEGAttentionFactor.PCAalphaGamma(idx_AttFactor);
        tmp2 = linearEffect;
        [r,p] = corrcoef(tmp1, tmp2, 'rows', 'complete');
        PMat(channels) = p(2);
        RMat(channels) = r(2);
    end
    figure; plot(PMat)
    
    % plot p-topography
    
    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    h = figure('units','normalized','position',[.1 .1 .4 .4]);
    cfg.zlim = [];
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = PMat';
    ft_topoplotER(cfg,plotData);
    
    %% plot linear effect topography
    
    scales = 8:18;
    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    h = figure('units','normalized','position',[.1 .1 .4 .4]);
    cfg.zlim = [-.1 .1];
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(nanmean(b(:,:,scales,17:25,2),4),5),3),1))';
    ft_topoplotER(cfg,plotData);

    
    %% Perform a cluster-based permutation test of this
    
    % time-varying correlation of 234-1
    
    for indTime = 1:size(mseavg.datBL,4)
        l1 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,indTime,1),4),5),3),2));
        l2 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,indTime,2),4),5),3),2));
        l3 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,indTime,3),4),5),3),2));
        l4 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,indTime,4),4),5),3),2));
        tmp1 = EEGAttentionFactor.PCAalphaGamma(idx_AttFactor);
        tmp2 = nanmean([l2,l3,l4],2)-l1;
        tmp2(4) = NaN; tmp1(37) = NaN;
        [r,p] = corrcoef(tmp1, tmp2, 'rows', 'complete')
        PMat(indTime) = p(2);
    end
    figure; plot(PMat)
    
    [h, p] = ttest(squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,channels,scales,17:25,1),4),5),3),2)), ...
        squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.dat(highChIdx,channels,scales,17:25,4),4),5),3),2)))
    
    figure;
    hold on;
    plot(squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(lowChIdx,channels,scales,:,1),5),3),2),1)));
    plot(squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(lowChIdx,channels,scales,:,2),5),3),2),1)));
    plot(squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(lowChIdx,channels,scales,:,3),5),3),2),1)));
    plot(squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(lowChIdx,channels,scales,:,4),5),3),2),1)));
    
    figure;
    hold on;
    plot(squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,1),5),3),2),1)));
    plot(squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,2),5),3),2),1)));
    plot(squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,3),5),3),2),1)));
    plot(squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,4),5),3),2),1)));
    
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
%cfg.zlim = [-3 3];


% putative rIFG signature: 10-20 Hz beta; channel peak @ 13,14

scales = 9:14;

h = figure('units','normalized','position',[.1 .1 .4 .4]);
    cfg.zlim = [-.3 .3];
    l1 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(highChIdx,:,scales,17:25,1),5),4),3),1));
    l2 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(highChIdx,:,scales,17:25,2),5),4),3),1));
    l3 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(highChIdx,:,scales,17:25,3),5),4),3),1));
    l4 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(highChIdx,:,scales,17:25,4),5),4),3),1));
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = nanmean([l1;l2;l3;l4],1)';
    %plotData.powspctrm = (nanmean([l2;l3;l4],1)-l1)';
    ft_topoplotER(cfg,plotData);
    title('Fast-scale MSE, 1 Target')
    
%% correlate with attentional modulation factor

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S14_Gamma/B_data/Z2_EEGAttentionFactor.mat')

h = figure('units','normalized','position',[.1 .1 .7 .7]);

    l1 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,51:60,1,17:25,1),5),4),3),2));
    l2 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,51:60,1,17:25,2),5),4),3),2));
    l3 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,51:60,1,17:25,3),5),4),3),2));
    l4 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,51:60,1,17:25,4),5),4),3),2));

    subplot(2,2,1); scatter(EEGAttentionFactor.PCAalphaGamma, nanmean([l2,l3,l4],2)-l1, 'filled');
    xlabel('Attention factor'); ylabel('Fast scale MSE 234-1');
    [r,p] = corrcoef(EEGAttentionFactor.PCAalphaGamma, nanmean([l2,l3,l4],2)-l1)
    legend(['Correlation: ', num2str(round(r(2),2)), ' p = ',num2str(round(p(2),3))])

    subplot(2,2,2); bar(nanmean([l1,l2,l3,l4],1))

    [~, p] = ttest(l1,l2)
    [~, p] = ttest(l1,l3)
    [~, p] = ttest(l1,l4)
    [~, p] = ttest(l1,nanmean([l2,l3,l4],2)); xlabel('Load'); ylabel('Fast MSE');

    %% fast scale r parameter is perfectly anticorrelated!

    l1 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,51:60,1,17:25,1),5),4),3),2));
    l2 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,51:60,1,17:25,2),5),4),3),2));
    l3 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,51:60,1,17:25,3),5),4),3),2));
    l4 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,51:60,1,17:25,4),5),4),3),2));

    subplot(2,2,3);
    scatter(EEGAttentionFactor.PCAalphaGamma, nanmean([l2,l3,l4],2)-l1, 'filled');
    xlabel('Attention factor'); ylabel('Fast scale R 234-1');
    [r,p] = corrcoef(EEGAttentionFactor.PCAalphaGamma, nanmean([l2,l3,l4],2)-l1)
    legend(['Correlation: ', num2str(round(r(2),2)), ' p = ',num2str(round(p(2),3))])

    subplot(2,2,4);
    bar(nanmean([l1,l2,l3,l4],1))

    [~, p] = ttest(l1,l2)
    [~, p] = ttest(l1,l3)
    [~, p] = ttest(l1,l4)
    [~, p] = ttest(l1,nanmean([l2,l3,l4],2)); xlabel('Load'); ylabel('Fast R');

set(findall(gcf,'-property','FontSize'),'FontSize',18)

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v2/C_figures/';
figureName = 'D_YA_FastScaleRelations';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% topography of baseline corrected fast scales

% pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
% pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
% addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
%cfg.zlim = [-3 3];

h = figure('units','normalized','position',[.1 .1 .4 .7]);
subplot(2,2,1);
    cfg.zlim = [-1 1];
    l1 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,:,1,17:25,1),5),4),3),1));
    l2 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,:,1,17:25,2),5),4),3),1));
    l3 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,:,1,17:25,3),5),4),3),1));
    l4 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,:,1,17:25,4),5),4),3),1));
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = l1';
    %plotData.powspctrm = (nanmean([l2;l3;l4],1)-l1)';
    ft_topoplotER(cfg,plotData);
    title('Fast-scale MSE, 1 Target')
subplot(2,2,2);
    cfg.zlim = [-.25 .25];
    l1 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,:,1,17:25,1),5),4),3),1));
    l2 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,:,1,17:25,2),5),4),3),1));
    l3 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,:,1,17:25,3),5),4),3),1));
    l4 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,:,1,17:25,4),5),4),3),1));
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = l1';
    %plotData.powspctrm = (nanmean([l2;l3;l4],1)-l1)';
    ft_topoplotER(cfg,plotData);
    title('Fast-scale R, 1 Target')
subplot(2,2,3);
    cfg.zlim = [-.1 .1];
    l1 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,:,1,17:25,1),5),4),3),1));
    l2 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,:,1,17:25,2),5),4),3),1));
    l3 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,:,1,17:25,3),5),4),3),1));
    l4 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.datBL(:,:,1,17:25,4),5),4),3),1));
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = (nanmean([l2;l3;l4],1)-l1)';
    ft_topoplotER(cfg,plotData);
    title('Fast-scale MSE, 234-1 Target')
subplot(2,2,4);
    cfg.zlim = [-.025 .025];
    l1 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,:,1,17:25,1),5),4),3),1));
    l2 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,:,1,17:25,2),5),4),3),1));
    l3 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,:,1,17:25,3),5),4),3),1));
    l4 = squeeze(nanmedian(nanmean(nanmean(nanmean(mseavg.rBL(:,:,1,17:25,4),5),4),3),1));
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = (nanmean([l2;l3;l4],1)-l1)';
    ft_topoplotER(cfg,plotData);
    title('Fast-scale R, 234-1 Target')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v2/C_figures/';
figureName = 'D_YA_FastScaleTopo';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% correlate with brainscores

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

load1 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,55:60,1:3,17:25,1),5),4),3),2));
load2 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,55:60,1:3,17:25,2),5),4),3),2));
load3 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,55:60,1:3,17:25,3),5),4),3),2));
load4 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.datBL(:,55:60,1:3,17:25,4),5),4),3),2));

subIdx = find(ismember(STSWD_summary.IDs,mseavg.SUBJ));

figure; scatter(STSWD_summary.brainscoreSummary.DDM_LV1(subIdx,5), nanmean([l1,l2,l3,l4],2), 'filled')
[r,p] = corrcoef(STSWD_summary.brainscoreSummary.DDM_LV1(subIdx,5), nanmean([l1,l2,l3,l4],2))

STSWD_summary.brainscoreSummary.SDTaskv2(STSWD_summary.brainscoreSummary.SDTaskv2==0) = NaN;

% fast-scale MSE correlates with SD BOLD brainscore

figure
subplot(1,2,1);
    a = nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,1:4),2);
    b = nanmean([load1,load2,load3,load4],2);
    scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
        'location', 'SouthEast'); legend('boxoff')
    title('fast-scale MSE correlates with SD BOLD brainscore')
    xlabel('Average SD BOLD brainscore (L1-4)'); ylabel('Average MSE, scale 1')
subplot(1,2,2);
    a = nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,2:4),2)-...
        nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,1),2);
    b = nanmean([load2,load3,load4],2)-load1;
    scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
        'location', 'SouthEast'); legend('boxoff')
    title('fast-scale MSE correlates with SD BOLD brainscore')


figure
    a = nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,1:4),2);
    b = nanmean([load1,load2,load3,load4],2);
    scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
        'location', 'SouthEast'); legend('boxoff')
    title('fast-scale MSE correlates with SD BOLD brainscore')
    xlabel('Average SD BOLD brainscore (L1-4)'); ylabel('Average entropy, scale 1')
    
figure
    a = nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(subIdx,1:4),2);
    b = nanmean([load1,load2,load3,load4],2);
    scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
        'location', 'SouthEast'); legend('boxoff')
    title('fast-scale MSE correlates with SD BOLD brainscore')
    
figure
    a = nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(subIdx,2:4),2)-...
        nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(subIdx,1),2);
    b = nanmean([load2,load3,load4],2)-load1;
    scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
        'location', 'SouthEast'); legend('boxoff')
    title('fast-scale MSE correlates with SD BOLD brainscore')

    figure
    a = nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,2:4),2)-...
        nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,1),2);
    b = nanmean([load2,load3,load4],2)-load1;
    scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
        'location', 'SouthEast'); legend('boxoff')
    title('fast-scale MSE correlates with SD BOLD brainscore')


figure; scatter(nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,2:4),2)-STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,1),...
    nanmean([l2,l3,l4],2)-l1, 'filled')
[r,p] = corrcoef(nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,2:4),2)-STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,1),...
    nanmean([l2,l3,l4],2)-l1, 'rows', 'complete')


% fast-scale MSE correlates inversely with PCA dim

figure; scatter(nanmean(STSWD_summary.PCAdim.dim(subIdx,1:4),2), nanmean([l1,l2,l3,l4],2), 'filled')
[r,p] = corrcoef(nanmean(STSWD_summary.PCAdim.dim(subIdx,1:4),2), nanmean([l1,l2,l3,l4],2), 'rows', 'complete')

% fast-scale MSE correlates positively with higher drift

figure; scatter(nanmean(STSWD_summary.HDDMwSess.driftEEG(subIdx,1:4),2), nanmean([l1,l2,l3,l4],2), 'filled')
[r,p] = corrcoef(nanmean(STSWD_summary.HDDMwSess.driftEEG(subIdx,1:4),2), nanmean([l1,l2,l3,l4],2), 'rows', 'complete')

figure; scatter(nanmean(STSWD_summary.DDM.driftEEG(subIdx,1:4),2), nanmean([l1,l2,l3,l4],2), 'filled')
[r,p] = corrcoef(nanmean(STSWD_summary.DDM.driftEEG(subIdx,1:4),2), nanmean([l1,l2,l3,l4],2), 'rows', 'complete')

% no change-change association

figure; scatter(nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,2:4),2)-...
    STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,1), ...
    nanmean([l2,l3,l4],2)-l1, 'filled')
[r,p] = corrcoef(nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,1:4),2), nanmean([l1,l2,l3,l4],2), 'rows', 'complete')

%% repeat correlations with r parameter

load1 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.rBL(:,44:60,1,17:25,1),5),4),3),2));
load2 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.rBL(:,44:60,1,17:25,2),5),4),3),2));
load3 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.rBL(:,44:60,1,17:25,3),5),4),3),2));
load4 = squeeze(nanmean(nanmean(nanmean(nanmean(mseavg.rBL(:,44:60,1,17:25,4),5),4),3),2));

figure
subplot(1,2,1);
    a = nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,1:4),2);
    b = nanmean([load1,load2,load3,load4],2);
    scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
        'location', 'SouthEast'); legend('boxoff')
    title('fast-scale R correlates with SD BOLD brainscore')
    xlabel('Average SD BOLD brainscore (L1-4)'); ylabel('Average R, scale 1')
subplot(1,2,2);
    a = nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,2:4),2)./...
        nanmean(STSWD_summary.brainscoreSummary.SDTaskv2(subIdx,1),2);
    b = nanmean([load2,load3,load4],2)-load1;
    scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
        'location', 'SouthEast'); legend('boxoff')
    title('fast-scale R correlates with SD BOLD brainscore')

figure
subplot(1,2,1);
    a = nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(subIdx,1:4),2);
    b = nanmean([load1,load2,load3,load4],2);
    scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
        'location', 'SouthEast'); legend('boxoff')
    title('fast-scale R does not correlate with Mean BOLD brainscore')
    xlabel('Average SD BOLD brainscore (L1-4)'); ylabel('Average R, scale 1')
subplot(1,2,2);
    a = nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(subIdx,2:4),2)-...
        nanmean(STSWD_summary.brainscoreSummary.meanTaskv2(subIdx,1),2);
    b = nanmean([load2,load3,load4],2)-load1;
    scatter(a, b, 70, 'k','filled'); l1 = lsline(); set(l1, 'LineWidth', 3);
    [r, p] = corrcoef(a(~isnan(a)), b(~isnan(a)));
    legend([l1], ['r = ', num2str(round(r(2),2)), ' p = ', num2str(round(p(2),4))],...
        'location', 'SouthEast'); legend('boxoff')
    title('fast-scale R does not correlate with Mean BOLD brainscore')


%% Occipital entropy increases --> PSD?

% scales = 1:18;
% figure; 
% subplot(2,2,1); imagesc(mseavg.time,[],squeeze(nanmedian(nanmean(mseavg.dat(:,channels,scales,:,4),2),1))); title('MSE, raw'); xlabel('Time (s)');
% yyaxis left; yticks(1:2:18); yticklabels({mseavg.freq(1:2:18)}); ylabel('LP-Freq')
% yyaxis right; yticks(1:2:18); yticklabels({mseavg.scale(1:2:18)}); ylabel('Scale')
% subplot(2,2,2); imagesc(mseavg.time,[],squeeze(nanmedian(nanmean(mseavg.datBL(:,channels,scales,:,4),2),1))); title('MSE, BL-corrected'); xlabel('Time (s)');
% yyaxis left; yticks(1:2:18); yticklabels({mseavg.freq(1:2:18)}); ylabel('LP-Freq')
% yyaxis right; yticks(1:2:18); yticklabels({mseavg.scale(1:2:18)}); ylabel('Scale')
% subplot(2,2,3); imagesc(mseavg.time,[],squeeze(nanmedian(nanmean(mseavg.r(:,channels,scales,:,4),2),1))); title('R, raw'); xlabel('Time (s)');
% yyaxis left; yticks(1:2:18); yticklabels({mseavg.freq(1:2:18)}); ylabel('LP-Freq')
% yyaxis right; yticks(1:2:18); yticklabels({mseavg.scale(1:2:18)}); ylabel('Scale')
% subplot(2,2,4); imagesc(mseavg.time,[],squeeze(nanmedian(nanmean(mseavg.rBL(:,channels,scales,:,4),2),1))); title('R, BL-corrected'); xlabel('Time (s)');
% yyaxis left; yticks(1:2:18); yticklabels({mseavg.freq(1:2:18)}); ylabel('LP-Freq')
% yyaxis right; yticks(1:2:18); yticklabels({mseavg.scale(1:2:18)}); ylabel('Scale')
% set(findall(gcf,'-property','FontSize'),'FontSize',18)

% Correlate R (bl-corr) with MSE (bl-corr) across subjects

% mseavg.rBL(isnan(mseavg.rBL)) = 0;
% mseavg.datBL(isnan(mseavg.datBL)) = 0;
% 
% for indChan = 1:60
%     for indScale = 1:18
%         for indTime = 1:36
%             for indCond = 1:4
%                 r = corrcoef(mseavg.rBL(:,indChan,indScale,indTime,indCond), mseavg.datBL(:,indChan,indScale,indTime,indCond));
%                 mseavg.corr(1,indChan,indScale,indTime,indCond) = r(2);
%             end
%         end
%     end
% end
% 
% scales = 1:18;
% channels = 44:60;
% figure; 
% imagesc(mseavg.time,[],squeeze(nanmedian(nanmean(mseavg.corr(:,channels,scales,:,4),2),1))); title('MSE, raw'); xlabel('Time (s)');
% yyaxis left; yticks(1:2:18); yticklabels({mseavg.freq(1:2:18)}); ylabel('LP-Freq')
% yyaxis right; yticks(1:2:18); yticklabels({mseavg.scale(1:2:18)}); ylabel('Scale')
% 
% scales = 11:14;
% channels = 1:60;
% tmp_MSE = squeeze(nanmean(nanmean(mseavg.datBL(:,channels,scales,mseavg.time> 6 & mseavg.time <6.5,4),2),3));
% tmp_r = squeeze(nanmean(nanmean(mseavg.rBL(:,channels,scales,mseavg.time> 6 & mseavg.time <6.5,4),2),3));
% figure; scatter(tmp_MSE, tmp_r)
% 
% 
% figure; imagesc(squeeze(nanmean(nanmean(mseavg.r(:,channels,scales,:,4),3)-...
%     nanmean(mseavg.r(:,channels,scales,:,1),3),2)))
% 
% figure; imagesc(squeeze(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,4),3)-...
%     nanmean(mseavg.datBL(:,channels,scales,:,1),3),1)))
% 
% figure; imagesc(squeeze(nanmean(nanmean(mseavg.r(:,channels,scales,:,4),3)-...
%     nanmean(mseavg.r(:,channels,scales,:,1),3),1)))
% 
% figure; imagesc(squeeze(nanmean(nanmean(mseavg.rBL(:,channels,scales,:,4),3),1)))
% figure; imagesc(squeeze(nanmean(nanmean(mseavg.datBL(:,channels,scales,:,4),3),1)))
% 
% figure; imagesc(squeeze(nanmean(nanmean(mseavg.datBL(:,channels,1:5,:,4),3),1)))
% figure; scatter(squeeze(nanmean(nanmean(mseavg.datBL(:,44:60,scales,16,4),3),2)),...
%     squeeze(nanmean(nanmean(mseavg.rBL(:,44:60,scales,16,4),3),2)))
% figure; scatter(squeeze(nanmean(nanmean(mseavg.datBL(:,44:60,1:5,16,4),3),2)),...
%     squeeze(nanmean(nanmean(mseavg.rBL(:,44:60,scales,16,4),3),2)), 'filled')
% 
% figure; scatter(squeeze(nanmean(nanmean(mseavg.datBL(:,:,scales,25,4),3),1)),...
%     squeeze(nanmean(nanmean(mseavg.rBL(:,:,scales,25,4),3),1)))
% 
% 
% figure; scatter(squeeze(nanmean(nanmean(nanmean(mseavg.datBL(:,:,scales,:,4),3),2),1)),...
%     squeeze(nanmean(nanmean(nanmean(mseavg.rBL(:,:,scales,:,4),3),2),1)))

%% channel*scale*time regressing out r from mse (across subjects)
% 17 subjects so far

for indID = 1:47
    for indChan = 1:60
        for indScale = 1:18
            %for indTime = 1:36
                for indCond = 1:4
                    X = [repmat(1,36,1), squeeze(mseavg.rBL(indID,indChan,indScale,:,indCond))];
                    y = squeeze(mseavg.datBL(indID,indChan,indScale,:,indCond));
                    [b, bint, residual] = regress(y,X);
                    mseavg.resid(indID,indChan,indScale,:,indCond) = residual;
                end
            %end
        end
    end
end


scales = 1:4;

figure; imagesc(squeeze(nanmedian(nanmedian(mseavg.resid(:,channels,scales,:,4),3)-...
    nanmedian(mseavg.resid(:,channels,scales,:,1),3),1)))

figure; imagesc(squeeze(nanmean(nanmean(mseavg.resid(:,channels,scales,:,4),3),1)))
figure; imagesc(squeeze(nanmean(nanmean(mseavg.resid(:,44:60,:,:,4),2),1)))
figure; imagesc(squeeze(nanmean(nanmean(mseavg.resid(:,44:60,:,:,4)-mseavg.resid(:,44:60,:,:,1),2),1)))

figure; imagesc(squeeze(nanmean(nanmean(mseavg.resid(:,44:60,scales,:,4),3))))
figure; imagesc(squeeze(nanmean(nanmean(mseavg.datBL(:,44:60,scales,:,4),3))))

figure; imagesc(corrcoef([squeeze(nanmean(nanmean(mseavg.datBL(:,44:60,scales,:,4),3))),...
    squeeze(nanmean(nanmean(mseavg.rBL(:,44:60,scales,:,4),3)))]))
figure; imagesc(corrcoef([squeeze(nanmean(nanmean(mseavg.dat(:,44:60,scales,:,4),3))),...
    squeeze(nanmean(nanmean(mseavg.r(:,44:60,scales,:,4),3)))]))

% R parameter and MSE estimate are positively coupled during STIM
% presentation in fast MSE bands (should reflect 1/f, spectral tilt)

scales = 12:15;
channels = [50:55];

figure; imagesc(corrcoef([squeeze(nanmean(nanmean(mseavg.dat(:,channels,scales,:,2),3))),...
    squeeze(nanmean(nanmean(mseavg.r(:,channels,scales,:,2),3)))]))


% MSE interindividually decouples, whereas R is unitary across time,
% correlated across STIM

figure; imagesc(squeeze(nanmean(nanmean(mseavg.resid(:,1:60,scales,:,4),3))))
figure; imagesc(-1.*squeeze(nanmean(nanmean(mseavg.rBL(:,1:60,scales,:,4),3))))
figure; imagesc(squeeze(nanmean(nanmean(mseavg.datBL(:,1:60,scales,:,4),3))))
figure; imagesc(squeeze(nanmedian(nanmedian(mseavg.dat(:,1:60,scales,:,4)-mseavg.dat(:,1:60,scales,:,1),3))))
figure; imagesc(squeeze(nanmedian(nanmedian(mseavg.r(:,1:60,scales,:,4)-mseavg.r(:,1:60,scales,:,1),3))))


% figure by scale across subjects, baseline-corrected

figure; 
subplot(2,1,1); imagesc(mseavg.time, [], squeeze(nanmean(nanmean(mseavg.datBL(:,1:60,:,:,4),2),1)))
subplot(2,1,2); imagesc(mseavg.time, [], squeeze(nanmean(nanmean(mseavg.rBL(:,1:60,:,:,4),2),1)))

figure; 
subplot(2,1,1); imagesc(mseavg.time, [], squeeze(nanmean(nanmean(mseavg.datBL(:,1:60,:,:,4),2),1))-squeeze(nanmean(nanmean(mseavg.datBL(:,1:60,:,:,1),2),1)))
subplot(2,1,2); imagesc(mseavg.time, [], squeeze(nanmean(nanmean(mseavg.rBL(:,1:60,:,:,4),2),1))-squeeze(nanmean(nanmean(mseavg.rBL(:,1:60,:,:,1),2),1)))

% show contrast raw

figure; 
subplot(2,1,1); imagesc(mseavg.time, [], squeeze(nanmean(nanmean(mseavg.dat(:,1:60,:,:,3),2),1))-squeeze(nanmean(nanmean(mseavg.dat(:,1:60,:,:,1),2),1)))
subplot(2,1,2); imagesc(mseavg.time, [], squeeze(nanmean(nanmean(mseavg.r(:,1:60,:,:,3),2),1))-squeeze(nanmean(nanmean(mseavg.r(:,1:60,:,:,1),2),1)))

%% plot mse & r

scales = find(mseavg.scale>=50 & mseavg.scale<=100);
channels = [44:60];

h = figure;
subplot(2,1,1);
    patches.timeVec = [-1.500 0 1.000 3.000 6.000 8.000 9.500];
    patches.colorVec = [1 1 1;.9 .9 .9; 1 1 1; 1 .9 .9; 1 1 1; .9 .9 .9];
    for indP = 1:numel(patches.timeVec)-1
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [.75 .75 1.2*[1 1]], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end; hold on;
    colorVec = brewermap(4, 'Dark2');
    for indCond = 1:4
        tmpData = squeeze(nanmedian(nanmedian(mseavg.datBL(:,channels,scales,:,indCond),3),2));
        tmpMeanData = squeeze(nanmedian(tmpData,1));
        tmpSEMData = std(tmpData,[],1)/sqrt(size(tmpData,1));
        lineProps.col = {colorVec(indCond,:)};
        l{indCond} = mseb(mseavg.time, tmpMeanData, tmpSEMData, lineProps, 1); hold on;
    end
    legend([l{1}.mainLine, l{2}.mainLine, l{3}.mainLine, l{4}.mainLine], {'dim1'; 'dim2'; 'dim3'; 'dim4'}); legend('boxoff');
    xticks(mseavg.time(1:10:end))
    title('MSEn during StateSwitch');
    xlim([-1.25,9.25]); %ylim([-.2 .2]);
    set(findall(gcf,'-property','FontSize'),'FontSize',16);
    xlabel('Time [s]'); ylabel('MSEn [a.u.]')
subplot(2,1,2);
    for indCond = 1:4
        tmpData = squeeze(nanmean(nanmean(mseavg.rBL(:,channels,scales,:,indCond),3),2));
        tmpMeanData = squeeze(nanmean(tmpData,1));
        tmpSEMData = std(tmpData,[],1)/sqrt(size(tmpData,1));
        lineProps.col = {colorVec(indCond,:)};
        l{indCond} = mseb(mseavg.time, tmpMeanData, tmpSEMData, lineProps, 1); hold on;
    end
    legend([l{1}.mainLine, l{2}.mainLine, l{3}.mainLine, l{4}.mainLine], {'dim1'; 'dim2'; 'dim3'; 'dim4'}); legend('boxoff');
    xticks(mseavg.time(1:10:end))
    title('R estimate during StateSwitch');
    xlim([-1.25,9.25]); %ylim([-.2 .2]);
    set(findall(gcf,'-property','FontSize'),'FontSize',16);
    xlabel('Time [s]'); ylabel('R estimate [a.u.]')
