function A_SS_mse_createCondData_dimension_v3()

% Function creates input data to MSE, segmented by dimensionality condition

% 180126 | JQK adapted to STSWD YA Study
% 180201 | added CSD transform as a preproc step
%        | updated FieldTrip version to make this work

%% set up paths
% restoredefaultpath
if ismac
    pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/';
    backend = 'local'; % local parfor
else
%     pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/';
%     backend = 'torque';
end
pn.dataIn   = [pn.root, 'X1_preprocEEGData/'];
pn.dataOut  = [pn.root, 'S4D_MSE_CSD_v4/B_data/B_MSE_Segmented_Dim_Input/']; mkdir(pn.dataOut);
pn.tools    = [pn.root, 'S4D_MSE_CSD_v4/T_tools/'];
addpath([pn.tools, 'fieldtrip-20170904/']);
ft_defaults

%% filenames

% N = 47 YAs + 53 OAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

%% preproc the data

fprintf('\n\nSubject directory: %s  . . .\n', pn.dataIn)
cd(pn.dataIn)

cond_names = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

for indID = 1:numel(IDs)
    % load dataset
    fprintf('Loading Subject %s: . . .\n', IDs{indID}(1:4))
    datfile = dir(sprintf('%s_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat', IDs{indID}(1:4)));
    if isempty(datfile);     break;            end
    cfg.inputfile = fullfile(pn.dataIn, datfile.name);
    Input = load(cfg.inputfile);
    for indCond = 1:4
        % select data from the condition of interest and save condition data
        cfg2 = []; cfg2.trials = find(nansum(Input.data.TrlInfo(:,1:4),2)==indCond);
        [dataCond] = ft_selectdata(cfg2, Input.data);
        dataCond.fsample = 500;
        TrlInfo = dataCond.TrlInfo;
        TrlInfoLabels = dataCond.TrlInfoLabels;
        data=dataCond; clear dataCond;
        cfg_old = data.cfg;
        % CSD transform
        csd_cfg = [];
        csd_cfg.elecfile = 'standard_1005.elc';
        csd_cfg.method = 'spline';
        % add .elec field
        load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat');
        data.elec = elec;
        data = ft_scalpcurrentdensity(csd_cfg, data);
        data.TrlInfo = TrlInfo; clear TrlInfo;
        data.TrlInfoLabels = TrlInfoLabels; clear TrlInfoLabels;
        data.cfg = cfg_old; clear cfg_old;
        data.cfg.csd = csd_cfg;
        % notch-filter the 8-15 Hz alpha band
        cfg_hpf.bsfilter = 'yes';
        cfg_hpf.bsfreq = [8 15];
        cfg_hpf.bsfiltord = 6;
        [data] = ft_preprocessing(cfg_hpf, data);
        save([pn.dataOut, sprintf('%s_%s_MSE_IN.mat', IDs{indID}(1:4), cond_names{indCond})],'data');
    end
end

end