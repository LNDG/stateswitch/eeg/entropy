% Probe parametric effect

clear all; clc; restoredefaultpath

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/';
pn.dataInOUT  = [pn.root, 'B_data/C_MSE_Output_v2/'];
addpath([pn.root, 'T_tools/fieldtrip-20170904/']);
addpath([pn.root, 'T_tools/']);
ft_defaults

% add convertPtoExponential
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/')
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')

cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

%% load data

load([pn.dataInOUT, 'mseavg_CSD_YA.mat'], 'mseavg');

% BLlims = [-.5 0];
% 
% mseavg.datBL=10*log10(mseavg.dat./repmat(nanmean(nanmean(mseavg.dat(:,:,:,find(mseavg.time>=BLlims(1) & mseavg.time<BLlims(2)),:),5),4), 1,1,1,numel(mseavg.time),4));
% mseavg.rBL=10*log10(mseavg.r./repmat(nanmean(nanmean(mseavg.r(:,:,:,find(mseavg.time>=BLlims(1) & mseavg.time<BLlims(2)),:),5),4), 1,1,1,numel(mseavg.time),4));

% BLlims = [2.5 2.8]; % within-condition mean baseline
% 
% load([pn.dataInOUT, 'mseavg_CSD_YA.mat'], 'mseavg');
% mseavg.datBL=10*log10(mseavg.dat./repmat(nanmean(mseavg.dat(:,:,:,find(mseavg.time>=BLlims(1) & mseavg.time<BLlims(2)),:),4), 1,1,1,numel(mseavg.time),1));
% mseavg.rBL=10*log10(mseavg.r./repmat(nanmean(mseavg.r(:,:,:,find(mseavg.time>=BLlims(1) & mseavg.time<BLlims(2)),:),4), 1,1,1,numel(mseavg.time),1));

% BLlims = [2.5 2.8]; % between-condition mean baseline
% 
% load([pn.dataInOUT, 'mseavg_CSD_YA.mat'], 'mseavg');
% mseavg.datBL=10*log10(mseavg.dat./repmat(nanmean(nanmean(mseavg.dat(:,:,:,find(mseavg.time>=BLlims(1) & mseavg.time<BLlims(2)),:),4),5), 1,1,1,numel(mseavg.time),4));
% mseavg.rBL=10*log10(mseavg.r./repmat(nanmean(nanmean(mseavg.r(:,:,:,find(mseavg.time>=BLlims(1) & mseavg.time<BLlims(2)),:),4),5), 1,1,1,numel(mseavg.time),4));


mseavg.datBL=mseavg.dat;
mseavg.rBL=mseavg.r;

%% create matrix for input to statistics

pn.elecData     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/';
load([pn.elecData, 'elec.mat']);

msestruct_dim = [];
for indCond = 1:4
    msestruct_dim{indCond}.label    = elec.label;
    msestruct_dim{indCond}.freq     = mseavg.scale; % freq actually represents the scale here
    msestruct_dim{indCond}.time     = mseavg.time;
    msestruct_dim{indCond}.msedatadimord   = 'subj_chan_freq_time';
    msestruct_dim{indCond}.msedata  = squeeze(mseavg.datBL(:,:,:,:,indCond));
    msestruct_dim{indCond}.elec     = elec;
end

%% parametric test at second level: 1st scale

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = elec.label;

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.frequency        = [1];
cfgStat.latency          = [2 8];
cfgStat.avgovertime      = 'no';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 1000;
cfgStat.parameter        = 'msedata';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, msestruct_dim{1});

subj = size(msestruct_dim{1}.msedata,1);
conds = 4;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat] = ft_freqstatistics(cfgStat, msestruct_dim{1}, msestruct_dim{2}, msestruct_dim{3}, msestruct_dim{4});

save([pn.root, 'B_data/E_statistics/L0_YA.mat'], 'stat', 'cfgStat');
load([pn.root, 'B_data/E_statistics/L0_YA.mat'], 'stat', 'cfgStat');

% figure; imagesc(squeeze(stat.posclusterslabelmat.*stat.stat))
% figure; imagesc(squeeze(stat.negclusterslabelsmat.*stat.stat))
% figure; imagesc(squeeze(stat.mask))

%% visualize topography and loading structure of load effect

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.style = 'both';
cfg.colormap = cBrew;
cfg.colorbar = 'EastOutside';
cfg.zlim = [-3 3];
cfg.highlight = 'on';
statsChans = find(squeeze(nanmean(nanmean(stat.mask(:,1,stat.time>3.5 & stat.time<6),3),2))>0);
cfg.marker = 'off';  
cfg.highlight = 'yes';
cfg.highlightchannel = stat.label(statsChans);
cfg.highlightcolor = [0 0 0];
cfg.highlightsymbol = '.';
cfg.highlightsize = 40;

h = figure('units','normalized','position',[.1 .1 .2 .2]); 
    plotData = [];
    plotData.label = stat.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(stat.stat(:,1,stat.time>3.5 & stat.time<6),3),2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'title'),'string','t values');
    pval = []; pval = convertPtoExponential(stat.posclusters(1).prob);
    title({'Linear effect entropy', ['p = ', pval{1}]});
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'L0_linearSE_topo';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

% h = figure('units','normalized','position',[.1 .1 .7 .3]);
% subplot(1,2,1);
%     plotData = [];
%     plotData.label = stat.label; % {1 x N}
%     plotData.dimord = 'chan';
%     plotData.powspctrm = squeeze(nanmean(nanmean(stat.stat(:,1,stat.time>3.5 & stat.time<6),3),2));
%     ft_topoplotER(cfg,plotData);
%     title({'Linear effect sample entropy';['linear effects in r: no cluster']})
% % plot temporal designation
% subplot(1,2,2); cla;
%     imagesc(stat.time,[],squeeze(nanmean(stat.stat(:,1,:),2)), 'AlphaData', .2); 
%     caxis([-5 5]) 
%     hold on;
%     imagesc(stat.time,[],squeeze(nanmean(stat.stat(:,1,:),2)), 'AlphaData', squeeze(nanmean(stat.mask(:,1,:),2))); 
%     xlabel('Time (s)'); ylabel('Channel');
%     cb = colorbar; set(get(cb,'label'),'string','t values');
%     line([3,3], [0,60], 'LineStyle', '--', 'Color', 'k', 'LineWidth', 2)
%     line([6,6], [0,60], 'LineStyle', '--', 'Color', 'k', 'LineWidth', 2)
%     title('Decrease in r during visual processing')
% set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% plot temporal traces

statsChans = find(squeeze(nanmean(nanmean(stat.mask(:,1,stat.time>3.5 & stat.time<6),3),2))>0);

addpath([pn.root, 'T_tools/shadedErrorBar']); % add shadedErrorBar
addpath([pn.root, 'T_tools/brewermap']); % add brewermap

% shading presents within-subject standard errors
% new value = old value ??? subject average + grand average
    
    h = figure('units','normalized','position',[.1 .1 .16 .18]); cla; hold on;
    
    % highlight different experimental phases in background
    patches.timeVec = [3 6]-3;
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.25 1];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    
    condAvg = squeeze(nanmean(nanmean(mseavg.datBL(:,statsChans,1,:,1:4),2),5));
    curData = squeeze(nanmean(mseavg.datBL(:,statsChans,1,:,1),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 6.*[.15 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(mseavg.datBL(:,statsChans,1,:,2),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 5.*[.2 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(mseavg.datBL(:,statsChans,1,:,3),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 3.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
    curData = squeeze(nanmean(mseavg.datBL(:,statsChans,1,:,4),2));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(mseavg.time-3,nanmean(curData,1),standError, ...
        'lineprops', {'color', 2.*[.3 .1 .1],'linewidth', 2}, 'patchSaturation', .25);
%     line([3 3], [0 .9], 'LineStyle', '--', 'Color', 'k')
%     line([6 6], [0 .9], 'LineStyle', '--', 'Color', 'k')
    xlim([-1 4]); ylim([.37 .46])%ylim([-.3 .85])
    
    sigVals = double(squeeze(nanmean(stat.mask,1)));
    sigVals(sigVals==0) = NaN;
    sigVals(sigVals>0) = 1;
    hold on; plot(stat.time-3, sigVals.*.38, 'k', 'linewidth', 5)
    
    xlabel('Time (s from stimulus onset)'); ylabel([{'Sample entropy'}]);
    leg = legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'1 Target', '2 Targets','3 Targets', '4 Targets'},...
        'orientation', 'vertical', 'location', 'North'); legend('boxoff')
    title({'Uncertainty increases sample entropy'; ''})
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    set(leg,'FontSize',15)
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'L0_EntropyTraces';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% extract values within cluster, correlate with 1/f changes, drift changes etc.
% calculate slopes first for each data point, then average (i.e. first level-within subject)

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/T_tools/winsor')

% b = []; b_r = [];
% for indID = 1:size(mseavg.datBL,1)
%     for indChan = 1:60
%         for indScale = 1%:numel(mseavg.scale)
%             for indTime = 1:numel(mseavg.time)
%                 X = [1,1;1,2;1,3;1,4];
%                 y = [squeeze(mseavg.datBL(indID,indChan,indScale,indTime,:))];
%                 y2 = [squeeze(mseavg.rBL(indID,indChan,indScale,indTime,:))];
%                 b(indID,indChan,indScale,indTime,:) = regress(y,X);
%                 b_r(indID,indChan,indScale,indTime,:) = regress(y2,X);
%             end
%         end
%     end
% end
% 
% individualMSE.data_slope(:,1) = squeeze(nanmean(nanmean(b(:,statsChans,1,mseavg.time>3 & mseavg.time<6,2),4),2));
% individualMSE.data_slope_win = winsor(individualMSE.data_slope, [0 95]);

individualMSE = [];
individualMSE.data = squeeze(nanmean(nanmean(mseavg.datBL(:,statsChans,1,mseavg.time>3 & mseavg.time<6,1:4),4),2));
X = [1 1; 1 2; 1 3; 1 4];
b=X\individualMSE.data';
individualMSE.data_slope(:,1) = b(2,:);
individualMSE.data_slope_win = winsor(individualMSE.data_slope, [0 95]);
individualMSE.IDs = mseavg.SUBJ;

save([pn.root, 'B_data/E_statistics/L0_linearTest_individualdata.mat'], 'individualMSE');


% check for outliers
figure; histogram(individualMSE.data_slope,10)
figure; histogram(individualMSE.data_slope_win,10)

load([pn.root, 'B_data/E_statistics/L1_AMF_regression_Residuals_individualdata.mat'], 'individualMSE_res');

figure; scatter(individualMSE.data_slope_win, individualMSE_res.data, 'filled')

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/STSWD_summary.mat')

idx_AttFactor = find(ismember(STSWD_summary.IDs, mseavg.SUBJ));
idx_MSE = find(ismember(mseavg.SUBJ,STSWD_summary.IDs));

% linear changes: SE & power factor
figure; scatter(individualMSE.data_slope_win(idx_MSE,1), STSWD_summary.EEG_LV1.slope(idx_AttFactor,1), 'filled')
[r, p] = corrcoef(individualMSE.data_slope_win(idx_MSE,1), STSWD_summary.EEG_LV1.slope(idx_AttFactor,1))

% linear changes: SE & 1/f
figure; scatter(individualMSE.data_slope_win(idx_MSE,1), STSWD_summary.spectralslope_linearChange(idx_AttFactor,1), 'filled')
[r, p] = corrcoef(individualMSE.data_slope_win(idx_MSE,1), STSWD_summary.spectralslope_linearChange(idx_AttFactor,1))

% average data: SE & 1/f
figure; scatter(nanmean(individualMSE.data(idx_MSE,1:4),2), nanmean(STSWD_summary.spectralslope(idx_AttFactor,1:4),2), 'filled')
[r, p] = corrcoef(nanmean(individualMSE.data(idx_MSE,1:4),2), nanmean(STSWD_summary.spectralslope(idx_AttFactor,1:4),2))

% no association of SE changes with behavioral changes

figure; scatter(individualMSE.data_slope_win(idx_MSE,1), STSWD_summary.HDDM_vt.driftEEG_linear(idx_AttFactor,1), 'filled')
[r, p] = corrcoef(individualMSE.data_slope_win(idx_MSE,1), STSWD_summary.HDDM_vt.driftEEG_linear(idx_AttFactor,1))

% no association of SE changes with pupil changes

figure; scatter(individualMSE.data_slope_win(idx_MSE,1), STSWD_summary.pupil2.stimdiff_slope(idx_AttFactor,1), 'filled')
    [r, p] = corrcoef(individualMSE.data_slope_win(idx_MSE,1), STSWD_summary.pupil2.stimdiff_slope(idx_AttFactor,1))
    