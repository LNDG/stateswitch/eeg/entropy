function mseavg = C_SS_mse_merge_YA_v2()

% merge data from alpha-bandstop mMSE analysis

%% set up paths
restoredefaultpath
if ismac
    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S4D_MSE_CSD_v4/';
    %pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S4D_MSE_CSD_v4/';
else
%     pn.root = '/home/mpib/kosciessa/';
%     backend = 'torque';
end
pn.dataInOUT  = [pn.root, 'B_data/C_MSE_Output_v2/'];

cond_names  = {'dim1'; 'dim2'; 'dim3'; 'dim4'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% N = 47 YAs;
IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
    '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
    '1261';'1265';'1266';'1268';'1270';'1276';'1281'};

SUBJ  = IDs;
nsub = length(SUBJ);

% load example mse to get nchan etc
subjdir = fullfile(pn.dataInOUT);
fprintf('Loading Subject %s Session %d: . . .\n', 1, 1)
cd(subjdir)
runname = sprintf('%s_%s_MSE_OUT.mat', IDs{1}(1:4), cond_names{1});
run = dir(runname);
load(run.name)

% JQK: dimord should be: 'chan_scales_time'
ntim = size(mse.sampen,3);
nchan = size(mse.sampen,1);
nscales = size(mse.sampen,2);

mseavg.dat = [];
mseavg.dat = NaN(nsub,nchan,nscales, ntim, 4); % subj sess channel scale time cond
mseavg.r = NaN(nsub,nchan,nscales, ntim, 4);
mseavg.time = mse.time;
mseavg.scale = mse.config.timescales;
mseavg.freq = mse.config.freqRequest;

% collect mse results across subjects etc.
for indID = 1:numel(IDs)
    for indCond = 1:4
        subjdir = fullfile(pn.dataInOUT);
        fprintf('\n\nSubject directory: %s  . . .\n', subjdir)
        fprintf('Loading Subject %s Condition %s: . . .\n', IDs{indID}(1:4), cond_names{indCond})
        runname = sprintf('%s_%s_MSE_OUT.mat', IDs{indID}(1:4), cond_names{indCond});
        run = dir(runname);
        if isempty(run)
            fprintf('%s not found\n', runname)
            continue
        end
        load(run.name)
        mseavg.dat(indID,:,:,:,indCond) = mse.sampen;
        mseavg.r(indID,:,:,:,indCond) = mse.r;
    end
end

mseavg.SUBJ = SUBJ;
mseavg.mse_leg = {'MSEn'};
mseavg.dimord = 'subj_sess_chan_scale_time_cond';
mseavg.dimordsize = size(mseavg.dat);

%% save

save([pn.dataInOUT, 'mseavg_CSD_YA.mat'], 'mseavg');
