function B_runMSE(indID, indCond)
% uses GitHub code dated 18.10.19

SetupIs = 'tardis';
%SetupIs = 'local';

if strcmp(SetupIs, 'tardis')
    pn.root = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S4D_MSE_CSD_v4/';
elseif strcmp(SetupIs, 'local')
    indID = '1117';
    indCond = 'dim4';
    pn.root = '/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S4D_MSE_CSD_v4/';
    addpath([pn.root, 'T_tools/mMSE-master'])
    %addpath([pn.root, 'T_tools/fieldtrip-20180227'])
    addpath('/Users/kosciessa/Desktop/mntTardisLNDG/LNDG/StateSwitch/WIP_eeg/S4D_MSE_CSD_v4/T_tools/fieldtrip-20180227/')
end
pn.dataIn = [pn.root, 'B_data/B_MSE_Segmented_Dim_Input/'];
pn.dataOut  = [pn.root, 'B_data/C_MSE_Output_v2/'];

%% select data

cfg_job.ID = indID;
cfg_job.cond = indCond;

load([pn.dataIn, sprintf('%s_%s_MSE_IN.mat', cfg_job.ID, cfg_job.cond)])

%% setup mMSE

% The function expects fieldtrip-style input:
% data.{trial{},elec,time{},label{},fsample,sampleInfo,trialinfo}

% within each trial the sorting is channel*trial

cfg_entropy=[];
cfg_entropy.toi      = -1.25:0.15:7.25;
cfg_entropy.winsize  = 0.5;
cfg_entropy.m        = 2; % pattern length
cfg_entropy.r        = 0.5; % similarity criterion
cfg_entropy.timescales = unique(ceil(10.^[0:.1:2]));
cfg_entropy.freqRequest = (1./cfg_entropy.timescales).*250;
cfg_entropy.coarsegrainmethod = 'filtskip';
cfg_entropy.filtmethod = 'lp';
cfg_entropy.recompute_r  = 'perscale_toi_sp';
cfg_entropy.polyremoval = 2;
cfg_entropy.mem_available = 8e9;
cfg_entropy.allowgpu = 0;

mse = ft_entropyanalysis(cfg_entropy, data);

%% save output

save([pn.dataOut, sprintf('%s_%s_MSE_OUT.mat', cfg_job.ID, cfg_job.cond)], 'mse', 'cfg_entropy', 'cfg_job')

end % function end