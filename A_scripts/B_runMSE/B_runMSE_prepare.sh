#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
pn.rootDir = '/home/mpib/LNDG/StateSwitch/WIP_eeg/S4D_MSE_CSD_v4/';
%% add fieldtrip toolbox for mMSE function
addpath([pn.rootDir, 'T_tools/fieldtrip-20170904/']); ft_defaults(); ft_compile_mex(true)
%% go to analysis directory containing .m-file
cd([pn.rootDir, 'A_scripts/B_runMSE/'])
%% compile function and append dependencies
mcc -m B_runMSE.m -a ./../../T_tools/mMSE-master/
exit